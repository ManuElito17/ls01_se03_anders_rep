import java.util.Scanner;

public class Arrys_�bungen {

	public static void main(String[] args) {
		// Aufgabe1();
		// Aufgabe2();
		// Aufgabe3();
		// Aufgabe4();

		int[] zahlen = intArray(10);

		//String zahlens = convertArrayToString(zahlen);
		//System.out.println(zahlens);
		
		zahlenUmdrehen(zahlen);
	}

	// Zahlen
	public static void Aufgabe1() {

		System.out.println("Aufgabe 1:");

		int[] ganzeZahlen = new int[10];

		for (int i = 0; i < ganzeZahlen.length; i++) {
			ganzeZahlen[i] = i;
			System.out.print(ganzeZahlen[i] + " ");
		}
		System.out.println(" ");

	}

	// ungerade zahlen
	public static void Aufgabe2() {

		System.out.println("Aufgabe 2:");

		int[] ungeradeZahlen = new int[10];

		for (int i = 0; i < ungeradeZahlen.length; i++) {
			ungeradeZahlen[i] = i + (i + 1);
			System.out.print(ungeradeZahlen[i] + " ");

		}
		System.out.println(" ");

	}

	// Palindrom
	public static void Aufgabe3() {
		System.out.println("Aufgabe 3:");

		char[] zahlen = new char[5];
		Scanner tastatur = new Scanner(System.in);
		int i;
		int a;

		for (i = 0; i < zahlen.length; i++) {
			zahlen[i] = tastatur.next().charAt(0);
		}
		for (a = (zahlen.length - 1); a >= 0; a--) {
			System.out.print(zahlen[a] + " ");
		}
		System.out.println(" ");
	}

	// Lotto
	public static void Aufgabe4() {

		System.out.println("Aufgabe 4a: ");

		int[] lottoZahlen = new int[] { 3, 7, 12, 18, 37, 42 };
		boolean zahl12 = false;
		boolean zahl13 = false;

		for (int i = 0; i < lottoZahlen.length; i++) {
			System.out.print(lottoZahlen[i] + " ");
		}
		System.out.println(" ");
		System.out.println("Aufgabe 4b: ");

		for (int i = 0; i < lottoZahlen.length; i++) {
			if (lottoZahlen[i] == 12) {
				zahl12 = true;
			} else {
			}
			if (lottoZahlen[i] == 13) {
				zahl13 = true;
			} else {
			}
		}
		if (zahl12 = true) {
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		}
		if (zahl13 = false) {
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
	}

	//Array erstellen
	public static int[] intArray(int gr��e) {
		int[] zahlen = new int[gr��e];
		for (int i = 0; i < zahlen.length; i++) {
			Scanner tastatur = new Scanner(System.in);
			zahlen[i] = tastatur.nextInt();

		}
		return zahlen;
	}

	// Array Helper
	public static String convertArrayToString(int[] zahlen) {
		String zahlens = Integer.toString(zahlen[0]);
		for(int i = 1; i<zahlen.length; i++) {
			zahlens =  zahlens + ", " + Integer.toString(zahlen[i]); 
		}

		return zahlens;
	}
		
	//zahlen umdrehen
	public static void zahlenUmdrehen(int[] zahlen) {
		int zwischenspeicher = 0;
		 for(int i = 0; i<=((zahlen.length-1)/2); i++) {
			 zwischenspeicher = zahlen[i];
			 zahlen[i] =  zahlen[(zahlen.length-1 - i)];
			 zahlen[zahlen.length -1 - i] = zwischenspeicher;
		 }
		 for(int i = 0; i<zahlen.length; i++) {
			 System.out.print(zahlen[i] + " ");
		 }
		 System.out.println("");
	}
	
}
