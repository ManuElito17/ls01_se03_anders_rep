import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	public Raumschiff() {
		
	}
	
	public Raumschiff(int photonentorpedoAnzahl,
						int energieversorgungInProzent, 
						int zustandSchildeInProzent, 
						int zustandHuelleInProzent,  
						int zustandLebenserhaltungssystemeInProzent, 
						int anzahlDroiden, 
						String schiffsname) {
		
	}
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	public void setEnergieversogungInProzent(int zustandEnergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}
	
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	public void setHuelleInProzent(int zustandHuelleInProzentNeu) {
		this.huelleInProzent = zustandHuelleInProzentNeu;
	}
	
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzentNeu;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void setAndroidenAnzahl(int androidenAnzahlNeu) {
		this.androidenAnzahl = androidenAnzahlNeu;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public void setSchiffsname(String schiffsnameNeu) {
		this.schiffsname = schiffsnameNeu;
	}
	
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(getPhotonentorpedoAnzahl() <1) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl()-1);
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(getEnergieversorgungInProzent() <50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			setEnergieversogungInProzent(getEnergieversorgungInProzent()-50);
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}
	
	private void treffer(Raumschiff r) {
		System.out.println(r + "wurde getroffen!");
		r.setSchildeInProzent(r.getSchildeInProzent()-50);
		if(r.getSchildeInProzent()<=0) {
			r.setHuelleInProzent(r.getHuelleInProzent()-50);
			r.setEnergieversogungInProzent(r.getEnergieversorgungInProzent()-50);
		}
		if(r.getHuelleInProzent()<=0) {
			r.setLebenserhaltungssystemeInProzent(0);
			r.nachrichtAnAlle("Lebvenserhaltungssysteme zerstört");
		}
	}
	
	public void nachrichtAnAlle(String message) {
		Raumschiff.broadcastKommunikator.add(message);
		System.out.println(message);		
	}
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		
		return this.broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		if(getPhotonentorpedoAnzahl()== 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("=*Click*=-");
		}else if(anzahlTorpedos > getPhotonentorpedoAnzahl()) {
			System.out.println(getPhotonentorpedoAnzahl() + " Photonentorpedo(s) eingesetzt");
		} else {
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() - anzahlTorpedos);
			System.out.println(anzahlTorpedos + "Photonentorpedo(s) eingesetzt");
		}
	}
	
	public void reperaturDurchführen(boolean schutzschilde,
									 boolean energieversorgung,
									 boolean schiffshuelle,
									 int anzahlDroiden)	{
		int schiffsstrukturen = 0;
		int reperatur = 0;
		Random zufall = new Random();
		int zufallszahl = zufall.nextInt(100);
				
		if(anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}
		if(schutzschilde == true) {
			schiffsstrukturen = schiffsstrukturen + 1;
		} else if(energieversorgung == true) {
			schiffsstrukturen = schiffsstrukturen + 1;
		} else if(schiffshuelle == true) {
			schiffsstrukturen = schiffsstrukturen + 1;
		}
		
		reperatur = (zufallszahl * anzahlDroiden) / schiffsstrukturen;
		if(schutzschilde == true) {
			setHuelleInProzent(getHuelleInProzent() + reperatur);
		} else if(energieversorgung == true) {
			setEnergieversogungInProzent(getEnergieversorgungInProzent() +reperatur);
		} else if(schiffshuelle == true) {
			setHuelleInProzent(getHuelleInProzent() + reperatur);
		}
	}	
	
	public void zustandRaumschiff() {
		System.out.println("Zustand Energieversorgung in Prozent: " + this.energieversorgungInProzent);
		System.out.println("Zustand Schilde in Prozent: " + this.schildeInProzent);
		System.out.println("Zustand Huelle in Prozent: " + this.huelleInProzent);
		System.out.println("Zustand Lebenserhaltungssysteme in Prozent: " + this.lebenserhaltungssystemeInProzent);	
	}
	
	public void ladungsverzeichnisAusgeben() {
		for(int i=0; i<ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i).toString());
		}
	}
	
	public void ladungsverzeichnisAufraeumen() {
		for(int i = 0; i<ladungsverzeichnis.size();i++) {
			if(ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
}
