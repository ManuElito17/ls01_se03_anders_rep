
public class Test {
	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff(0, 100, 100, 100, 100, 200, "klingonen");
		Ladung l1 = new Ladung("ladung1", 5);
		Ladung l2 = new Ladung("ladung2", 2);
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);
		klingonen.ladungsverzeichnisAusgeben();
	
	}
}
