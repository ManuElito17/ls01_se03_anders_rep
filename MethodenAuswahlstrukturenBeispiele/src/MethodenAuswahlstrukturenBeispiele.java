import java.util.Scanner;

public class MethodenAuswahlstrukturenBeispiele {

	public static void main(String[] args) {
		
		int minZahl = min(12,9);
		int maxZahl = max(891,85);
		int erg = doit(3,7,'+');
		/*System.out.println("Das Ergebnis der Summe ist " + erg);
		erg = doit(3,7,'-');
		System.out.println("Das Ergebnis der Subtraktion ist " + erg);
		erg = doit(3,7,'?');
		System.out.println("Falscher Operator " + erg);
		System.out.println("Die kleinere Zahl ist " + minZahl);
		System.out.println("Die gr��ere Zahl ist " + maxZahl);
		zaehlhoch();
		zaehlrunter(); 
		sortiere_abc();
		schaltjahr();*/
		//quadrat();
		//treppeabfallend();
		treppeaufsteigend();

	}

	
	public static int min (int zahl1, int zahl2) {
		
		if(zahl1 < zahl2) {
			
			return zahl1;
		} else {
			
			return zahl2;
		}
	}
	
	public static int max(int zahl1, int zahl2) {
		
		if(zahl1 > zahl2) {
			
			return zahl1;
		} else {
			
			return zahl2;
		}
	}
	
	public static int doit(int zahl1, int zahl2, char operator) {
		
		int ergebnis;
		
		if(operator == '+') {
			ergebnis = zahl1 + zahl2;
		} else if (operator == '-') {
			
			ergebnis = zahl1 - zahl2;
		} else {
			ergebnis = -999;
		}
		
		return ergebnis;
	}
	
	public static void zaehlhoch() {
	
		int n=0;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Wie hoch soll gez�hlt werden?: ");
		n = tastatur.nextInt();
		
		for(int i=1; i<=n; i++) {
			
			System.out.println(i);
		}	
	}
	
	public static void zaehlrunter() {
	
		int n=0;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Von welcher Zahl soll runter gez�hlt werden?: ");
		n = tastatur.nextInt();
		
		for(int i=n; i>0; i--) {
			
			System.out.println(i);
		}
	}
	
	
	public static void sortiere_abc() {
		
		char a;
		char b;
		char c;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben sie den ersten Wert ein:");
		a = tastatur.next().charAt(0);
		System.out.println("Geben sie den zweiten Wert ein:");
		b = tastatur.next().charAt(0);
		System.out.println("Geben sie den dritten Wert ein:");
		c = tastatur.next().charAt(0);
		
		
		if(a>b && a>c) {
			if(b>c) {
				System.out.println("Die richtige Reihenfolgfe ist: " + c + b + a);
			} else {
				System.out.println("Die richtige Reihenfolgfe ist: " + b + c + a);
			}
		} else if(b>a && b>c) {
			if(a>c) {
				System.out.println("Die richtige Reihenfolgfe ist: " + c + a + b);
			} else {
				System.out.println("Die richtige Reihenfolgfe ist: " + a + c + b);
			}
		} else {
			if(a>b) {
				System.out.println("Die richtige Reihenfolgfe ist: " + b + a + c);
			} else {
				System.out.println("Die richtige Reihenfolgfe ist: " + a + b + c);
			}
		}
		
	}
	
	public static void schaltjahr() {
		int jahr = 0;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie ein jahr ein: ");
		jahr = tastatur.nextInt();
	
		//int regel1 = jahr/100;
		//regel1 = regel1*4
		int regel2 = jahr/100;
		regel2 = regel2*100;
		int regel3 = jahr/400;
		regel3 = regel3*400;
		if(jahr % 4 == 0) {
			if(regel2 == jahr) {
				if(regel3 == jahr) {
					System.out.println("Es ist ein Schaltjahr.");
				}else {
					System.out.println("Es ist kein Schaltjahr.");
				}
			}else {
				System.out.println("Es ist ein Schaltjahr.");
			}
		} else {
		System.out.println("Es ist kein Schaltjahr.");
		}
	}	
	
	
	public static void quadrat() {
		
		int laenge1 = 0;
		int laenge2 = 0;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben sie die Seitenl�ge des Quadrats an: ");
		laenge1 = tastatur.nextInt();
		System.out.println("Geben sie die Seitenl�ge des Quadrats an: ");
		laenge2 = tastatur.nextInt();
		
		for(int i=0; i<laenge1-1; i++) {
			
			System.out.print("* ");
	
		}
		System.out.println("* ");
		for(int a=0; a<laenge2-2; a++) {
			System.out.print("* ");
			for(int b=0; b<laenge1-2; b++) {
				System.out.print("  ");
			}
			System.out.println("* ");
		}
		for(int c=0; c<laenge1; c++) {
			System.out.print("* ");
		}
		
	}
	
	
	public static void treppeabfallend() {
		
		int hoehe = 0;
		int stufenbreite = 0;
		int a = 0;
		int b = 0;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Treppenh�he: ");
		hoehe = tastatur.nextInt();
		System.out.println("Stufenbreite: ");
		stufenbreite = tastatur.nextInt();
		
		for(int i=0; i<hoehe; i++) {
			 
			a = a + 1;
			b = a * stufenbreite;
			
			for (int c=b; c>0; c--) {
			
				System.out.printf("* ");
			}
			System.out.println(" ");
			
		}
		
	}
	
	
	public static void treppeaufsteigend() {
		
		int hoehe = 0;
		int stufenbreite = 0;
		int a = 0;
		int b = 0;
		int c = 0;
		int d = 0;
		int e = 0;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Treppenh�he: ");
		hoehe = tastatur.nextInt();
		System.out.println("Stufenbreite: ");
		stufenbreite = tastatur.nextInt();
		d = hoehe;
		
		for(int i=0; i<hoehe; i++) {
			a = a + 1;
			b = a * stufenbreite;
			d = d - 1;
			
			for( c=b ; c>0; c--) {
				
				
				System.out.print("*");
				
			}
			System.out.println(" ");
		}
		
	
	}
}
