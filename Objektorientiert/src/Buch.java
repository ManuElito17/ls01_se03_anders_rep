
public class Buch {

	private String titel;
	private double preis;
	
	
	public Buch() {
		this.titel = "Unbekannt";
		this.preis = 0.0;
	}
	
	public  String getTitel() {
		return this.titel;
	}
	
	public void setTitel(String titelNeu) {
		this.titel = titelNeu;
	}
	
	
}
