import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {

	public static ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();
	
	public static void main(String[] args) {
		
		
		int auswahl;
		
		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				benutzerAnzeigen();
				break;
			case 2:
				benutzerErfassen();
				break;
			case 3:
				benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");				
			}
		}while (true );
		
	}
	
	public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("     Benutzerverwaltung     ");
        System.out.println("----------------------------\n");
        System.out.println("1 - Benutzer anzeigen");
        System.out.println("2 - Benutzer erfassen");
        System.out.println("3 - Benutzer löschen");
        System.out.println("4 - Ende");
         
        System.out.print("\nEingabe:  ");
        selection = input.nextInt();
        return selection;    
    }
	
	public static void benutzerAnzeigen() {
		int a =0;
		for(int i = 0; i<benutzerliste.size(); i++) {
			a = i+1;
			System.out.println("[" + a +"]" + benutzerliste.get(i).toString());
		}
	}
	
	public static void benutzerErfassen() {
		String name;
        int benutzerNummer;
        Scanner Eingabe = new Scanner(System.in);
        System.out.println("Name eingeben:");
        name = Eingabe.nextLine();
        System.out.println("Benutzernummer eingeben:");
        benutzerNummer = Eingabe.nextInt();
        Benutzer b1 = new Benutzer(name, benutzerNummer);
        benutzerliste.add(b1);
	}
	
	public static void benutzerLöschen() {
		Scanner Eingabe = new Scanner(System.in);
		int benutzerNummer = 0;
		System.out.println("Welchen Benutzer löschen? ");
		benutzerNummer = Eingabe.nextInt();
		
		benutzerliste.remove(benutzerNummer-1);
	}
}
