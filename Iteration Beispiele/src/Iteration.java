import java.util.Scanner;

public class Iteration {

	public static void main(String[] args) {

		// uebung();
		matrix();

	}

	public static void uebung() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben sie bitte n ein: ");
		int n = tastatur.nextInt();
		int zaehler = 0;
		int ergebnis = 0;
		while (zaehler <= n) {
			ergebnis = ergebnis + zaehler;
			zaehler++;
		}
		System.err.println("Summe: " + ergebnis);

	}

	public static void matrix() {

		int eingabe = 0;
		int zahl = 1;
		int hilfszahl = 10;
		int quer1 = 0;
		int quer2 = 0;
		int quersumme;
		Scanner tastatur = new Scanner(System.in);

		System.out.println("Geben sie eine Zahl zwischen 2 und 9 ein: ");
		eingabe = tastatur.nextInt();

		while (zahl < 100) {
			while (zahl < hilfszahl) {

				quer1 = zahl;
				quer1 = quer1 % 10;
				quer2 = (zahl - quer1)/10;
				quer2 = quer2 % 10;
				quersumme = quer1 + quer2;

				if (((zahl % eingabe == 0) && (zahl != 0))) {
					System.out.print("*  ");
				} else if (quersumme == eingabe) {
					System.out.print("*  ");

				} else {
					System.out.print(zahl + " ");
				}
				zahl++;

			}
			hilfszahl = hilfszahl + 10;
			System.out.println(" ");

		}

	}

}
