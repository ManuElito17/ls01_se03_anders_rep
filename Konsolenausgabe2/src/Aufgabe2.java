
public class Aufgabe2 {

	public static void main(String[] args) {
		// Aufgabe 2
		
		//1. Zeile
		/*System.out.printf("%-5s","0! ");
		System.out.print("=");
		System.out.printf("%-19s", "");
		System.out.print("=");
		System.out.printf("%4s", "1");*/
		
		System.out.printf("%-5s= %-19s=%4s","0!","" ,1);
		System.out.printf("%n%-5s= %-19s=%4s","1!", 1,1);
		System.out.printf("%n%-5s= %-19s=%4s","2!", "1 * 2",2);
		System.out.printf("%n%-5s= %-19s=%4s","3!", "1 * 2 * 3",6);
		System.out.printf("%n%-5s= %-19s=%4s","4!", "1 * 2 * 3 * 4",24);
		System.out.printf("%n%-5s= %-19s=%4s","5!", "1 * 2 * 3 * 4 * 5", 120);

	}

}
