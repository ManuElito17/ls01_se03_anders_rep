import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
	  
      //double x=0.00;
      double x = eingabe("Geben sie den 1. Wert ein ");
     // double y = 0.00;
     double  y = eingabe("Geben sie den 2. Wert ein ");
      double m = berechneMittelwert(x,y);
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      // m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
     ausgabe(x,y,m);
   }
   
   public static double berechneMittelwert (double wertx, double werty) {
	   
	   double m = (wertx + werty) / 2.0;
	   
	   return m;
   }
   
   public static void ausgabe (double wertx, double werty, double berechnetteMitte) {
	   
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", wertx, werty, berechnetteMitte);
	   
	  
   }
   
   public static double eingabe(String text) {
	   System.out.print(text );
	  Scanner tastatur = new Scanner(System.in);
	   double wert = tastatur.nextDouble();
	   
	   return wert;
   }
   
}