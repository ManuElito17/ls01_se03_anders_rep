
import java.util.Scanner;

public class PCHaendler {

  //private static Scanner myScanner;
  public static void main(String[] args) {
    
    // Benutzereingaben lesen
    
    String artikel = liesString("Was m�chten Sie bestellen?");
    int anzahl = liesInt("Wie viel m�chten Sie bestellen?");
    double nettopreis = liesDouble("Geben Sie den Nettopreis ein:");
    double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
    
    
    // Verarbeiten
    double nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
    double bruttogesamtpreis = berechneGesamtnettopreis(nettogesamtpreis, mwst);

    // Ausgeben

     rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
    
  }

  public static String liesString(String text) {
    Scanner myScanner = new Scanner(System.in);
    System.out.println(text);
    String artikel = myScanner.next();
    return artikel;
    
  }
  
  public static int liesInt(String text) {
	    Scanner myScanner = new Scanner(System.in);
	    System.out.println(text);
	    int anzahl = myScanner.nextInt();
	    return anzahl;
	    
	  }
  
  public static double liesDouble(String text) {
	    Scanner myScanner = new Scanner(System.in);
	    System.out.println(text);
	    double preis = myScanner.nextDouble();
	    return preis;
	    
	  }
  
  public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
	  double gesamtNettopreis = anzahl * nettopreis;
	  return  gesamtNettopreis;
  }
  
  public static double berechneGesamtnettopreis(double nettogesamtpreis, double mwst) {
	  double gesamtBrutopreis = nettogesamtpreis * (1 + mwst / 100);
	  return gesamtBrutopreis;
  }
  
  public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
	  System.out.println("\tRechnung");
	  System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	  System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
  
  }
}