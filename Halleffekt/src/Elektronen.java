import java.applet.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.lang.Math.*;
//import javafx.util.Pair;

/*
public class Zeichenfenster{
  
  public static void main(){
    
    JFrame F = new JFrame();
    F.setSize(400,600);
    F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    F.setVisible(true);
    
    Graphics stift = F.getGraphics();
    
    
    
    
  }
  
}
*/
  

public class Elektronen{
   
  //
  //GLOBALE VARIABLEN
  //
  
  //Gr��e der Platte
  static int h = 500;
  static int b0 = 800;
  static int b;
  
  //Koordinaten
  static double x = 0.1;
  static double y = 0.1;
    
  //alte Koordinaten
  static double xAlt = 0.1;
  static double yAlt = 0;
  
  //Anfangs-y
  static double yAnf;
  
  //maximales y f�r oberen Halbkreis
  static double yMax = 0;
  
  //Probetestwert gegen Fehler
  static double f = 0.1;
  
  //Z�hlvariable
  static double i = 0;
  
  //i nur halt nicht schneller, um Kraftpfeile gleichm��ig anwachsen zu lassen
  static double w;
  
  //letzter w-Wert f�r Kraftvektor der elektrischen Feldst�rke
  static double wMax;
  
  //% von w zu wMax
  static int wProz; 
    
  //Magnetfeldst�rke B 
  static int B;
  
  //% von B von B=20 (maximal)
  static int BProz;
  
  //Stromst�rke I
  static int I;
  
  //Kr�ftepfeile an/aus
  static boolean vec;
  
  //Raster an/aus
  static boolean ras;
  
  //Raster nur zeichnen, wenn radiert wird
  static boolean rad = false;
  
  public static void main(){
    
    JFrame F = new JFrame();
    F.setSize(b0,h);
    F.setBackground(Color.white);
    F.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    F.setVisible(true);
    
    //stift zum �bergeben festlegen
    Graphics stift = F.getGraphics();
    
    //Graphen zeichnen
    rechnen(stift);
    
    
    
    
    
    
    //Warteschleife mit try/catch f�r m�gliche Fehler (sonst nicht ausgef�hrt) 
    try{
      Thread.sleep(5000);
    }catch(InterruptedException fehlerdingsiDasAnscheinendDochMehrmalsAuftauchtIchJedochNichtNormalBenennenWerdeWeilEsKeinenUnterschiedMacht){}
    
  }
  
  
  
  public static void rechnen(Graphics stift){
    
    //VARIABLEN, DIE SP�TER �BERGEBEN WERDEN SOLLEN
    B = Integer.parseInt(Gitterformatierung.Magnetfeld);
    I = Integer.parseInt(Gitterformatierung.ElektrischesFeld);
    vec = Gitterformatierung.VektorBox.isSelected();
    ras = Gitterformatierung.RasterBox.isSelected();
    
    //Stromst�rke zu passenderem Geschwindigkeitsfaktor �ndern
    I = (int)Math.round(I/5)+1;
    
    //Bedingung: Kr�ftepfeile nur, wenn aktiviert
    if(vec==true){
      
      //Breite so setzen, dass Kr�ftepfeile eingef�gt werden k�nnen und nicht mit Graphen kollidieren
      b=b0*20/21;
      
      //% von B von B=20 (maximal) umrechnen
      BProz = B * 100 / 20;
      
      
      //Rand f�r Kraftvektoren
      stift.setColor(Color.black);
      stift.drawLine(b+1,0,b+1,h);
      stift.drawLine(b+1,h/2,b+(b/20),h/2);
      
      //zeichnen von Kraftvektor f�r Magnetfeldst�rke
      stift.setColor(Color.red);
      stift.drawLine(b+(b/40), h/2, b+(b/40), (h/2)-((h/2)*BProz/100));
      stift.drawLine(b+(b/40), (h/2)-((h/2)*BProz/100), b+(b/80), ((h/2)-((h/2)*BProz/100))+(b/80));
      stift.drawLine(b+(b/40), (h/2)-((h/2)*BProz/100), b+3*(b/80), ((h/2)-((h/2)*BProz/100))+(b/80));
      
    }else if(vec==false){
      b=b0;
    }    
    
    
    // VORERST NICHT ERFOLGREICH
    //Radius (angepasst an Magnetfeldst�rke, wobei der Kreis bei 20 am kleinsten und bei 1 am gr��ten ist)
    double r = (h/2) + (10 - B*10); 
    
    //Radius
    //double r = h/2;
    
    //Raster zeichnen
    raster(h,b,stift);
    
    //
    //RECHNEN
    //
    
    yAnf = r - (10 - B*10);
    
    //letzten i-Wert f�r Kraftvektor der elektrischen Feldst�rke berechnen
    wMax = 0;
    i = 1;
    y = 0.1;
    f = 0.1;
    
    //wMax berechnen
    while( (int)Math.round(yAnf) > (int)Math.round(y) ){
      
      //zeichnen ohne Graphen darzustellen und nur f�r letzten x-Wert
      f = Math.sqrt( Math.pow(r,2) - Math.pow(b/i,2)) - (10 - B*10);
      if(f>=0 && f<=h){
        
        //y-Koordinate zu x-Wert errechnen
        y = Math.sqrt( Math.pow(r,2) - Math.pow(b/i,2)) - (10 - B*10);
        
      }
      
      //i mit angepasster Schrittweite vergr��ern
      i=iBerechnung(i);        
      
      wMax=wMax+1;
      
    }  
    System.out.println("wMax " + wMax);
    
    i = 1;
    
    y = 0.1;
    f = 0.1;
    w = 0;
    
    //zeichnen
    while( (int)Math.round(yAnf) > (int)Math.round(y) ){
      System.out.println("i " + i);
      stift.setColor(Color.black);
      zeichnen(r, i, stift);
      
      //Warteschleife mit try/catch f�r m�gliche Fehler (sonst nicht ausgef�hrt) 
      try{
        Thread.sleep(200/I);
      }catch(InterruptedException fehlerdingsiDasSowiesoNiewiederVerwendetWirdUndDasSichDeshalbNichtMitAnderenNamenUeberschneidenSollUndJaIchFindeDasIrgendwieWitzigUndHoffeIchLoescheDasNoch){}
      
      //entfernen
      stift.setColor(Color.white);
      zeichnen(r, i, stift);    
      System.out.println("RAZZEL");
      
      
      //i mit angepasster Schrittweite vergr��ern
      i=iBerechnung(i);        
      
      w=w+1;
      
      System.out.println("w " + w);
      System.out.println(y);
    }
    
    //letzen Graphen neu zeichnen, da er sonst gel�scht wird
    stift.setColor(Color.black);
    stift.drawLine(0, (int)Math.round(yAnf), b, (int)Math.round(yAnf));
    stift.drawLine(0, (int)Math.round(yAnf)+1, b, (int)Math.round(yAnf)+1);
    stift.drawLine(0, (int)Math.round(yAnf)-1, b, (int)Math.round(yAnf)-1);    
    
    //Kr�ftepfeil nur wenn aktiviert
    if(vec==true){
      
      //letzten Kraftvektor von elektrischer Feldst�rke neu zeichnen, da er sonst gel�scht wird
      stift.setColor(Color.blue);
      stift.drawLine(b+(b/40), h/2, b+(b/40), (h/2)+(((h/2)*BProz/100)*wProz/100));
      stift.drawLine(b+(b/40), (h/2)+(((h/2)*BProz/100)*wProz/100), b+(b/80), ((h/2)+(((h/2)*BProz/100)*wProz/100))-(b/80));
      stift.drawLine(b+(b/40), (h/2)+(((h/2)*BProz/100)*wProz/100), b+3*(b/80), ((h/2)+(((h/2)*BProz/100)*wProz/100))-(b/80));
      
      //letzten Kraftvektor von Magnetfeldst�rke neu zeichnen, da durchschneidungen sonst eventuell wieder sichtbar
      stift.setColor(Color.red);
      stift.drawLine(b+(b/40), h/2, b+(b/40), (h/2)-((h/2)*BProz/100));
      stift.drawLine(b+(b/40), (h/2)-((h/2)*BProz/100), b+(b/80), ((h/2)-((h/2)*BProz/100))+(b/80));
      stift.drawLine(b+(b/40), (h/2)-((h/2)*BProz/100), b+3*(b/80), ((h/2)-((h/2)*BProz/100))+(b/80));
      
    }
    
  }
    
    
  private static void zeichnen(double r, double i, Graphics stift){
    
    //pr�fen, ob radiert oder gezeichnet wird (f�r Raster)
    if(stift.getColor()==Color.white){
      rad = true;
    }else if(stift.getColor()==Color.black){
      rad = false;
    }
    
    //zur�cksetzen, um Fehler zu vermeiden
    y = 0.1;
    f = 0.1;
    yMax = 0;
    
    //Graphen einmal zeichnen
    for(x=0; x<=b && f>=0 && f<=h; x++){
      
      //alte Koordinaten festlegen
      xAlt = x;
      yAlt = y;
      
      //Proberechnung, dass nicht unter 0
      f = Math.sqrt( Math.pow(r,2) - Math.pow(x/i,2)) - (10 - B*10);
      if(f>=0 && f<=h){
        
        //y-Koordinate zu x-Wert errechnen
        y = Math.sqrt( Math.pow(r,2) - Math.pow(x/i,2)) - (10 - B*10);
        //zeichnen
        stift.drawLine( (int)Math.round(xAlt), (int)Math.round(yAlt), (int)Math.round(x), (int)Math.round(y));
        //Graph dicker zeichnen
        stift.drawLine( (int)Math.round(xAlt)+1, (int)Math.round(yAlt)+1, (int)Math.round(x)+1, (int)Math.round(y)+1);
        stift.drawLine( (int)Math.round(xAlt)-1, (int)Math.round(yAlt)-1, (int)Math.round(x)-1, (int)Math.round(y)-1);
        
        
        
      }
      
      //letzter y-Wert bleibt erhalten --> yMax
      yMax = y;
      
    }
    
    
    //pr�fen, ob Graph am rechten Rand angekommen ist --> keine Spiegelung am rechten Rand
    if(x<b){
      
      //zur�cksetzen, um Fehler zu vermeiden
      y = 0.1;
      f = 0.1;
      
      //oberen Halbkreis des Graphen zeichnen
      for(x=0; x<=b && f>=0 && f<=h; x++){
        
        //alte Koordinaten festlegen
        xAlt = x;
        yAlt = y;
        
        //Proberechnung, dass nicht unter 0
        f = Math.sqrt( Math.pow(r,2) - Math.pow(x/i,2)) - (10 - B*10);
        if(f>=0 && f<=h){
          
          //y-Koordinate zu x-Wert errechnen
          y = Math.sqrt( Math.pow(r,2) - Math.pow(x/i,2)) - (10 - B*10);
          //zeichnen
          stift.drawLine( (int)Math.round(xAlt), (int)Math.round(-yAlt + (2*yMax)), (int)Math.round(x), (int)Math.round(-y + (2*yMax)));
          //Graph dicker zeichnen
          stift.drawLine( (int)Math.round(xAlt)+1, (int)Math.round(-yAlt + (2*yMax))+1, (int)Math.round(x)+1, (int)Math.round(-y + (2*yMax))+1);
          stift.drawLine( (int)Math.round(xAlt)-1, (int)Math.round(-yAlt + (2*yMax))-1, (int)Math.round(x)-1, (int)Math.round(-y + (2*yMax))-1);
          
          
        }
        
      }  
      
    }
    
    //% von w zu wMax
    wProz = (int)Math.round( w * 100 / wMax ); 
    
    //wegradieren nicht verhindern
    if(stift.getColor()!=Color.white){
      stift.setColor(Color.blue);
    }
    
    //Kr�ftepfeil nur wenn aktiv
    if(vec==true){
      
      //zeichnen von Kraftvektor von elektrischer Feldst�rke
      stift.drawLine(b+(b/40), h/2, b+(b/40), (h/2)+(((h/2)*BProz/100)*wProz/100));
      stift.drawLine(b+(b/40), (h/2)+(((h/2)*BProz/100)*wProz/100), b+(b/80), ((h/2)+(((h/2)*BProz/100)*wProz/100))-(b/80));
      stift.drawLine(b+(b/40), (h/2)+(((h/2)*BProz/100)*wProz/100), b+3*(b/80), ((h/2)+(((h/2)*BProz/100)*wProz/100))-(b/80));
      
      //nachzeichnen von Trennlinie, weil durchschneiden m�glich
      stift.setColor(Color.black);
      stift.drawLine(b+1,h/2,b+(b/20),h/2);
      stift.drawLine(b+1,0,b+1,h);
      
      //nachzeichnen von Kraftvektor f�r Magnetfeldst�rke, weil durchschneiden m�glich
      stift.setColor(Color.red);
      stift.drawLine(b+(b/40), h/2, b+(b/40), (h/2)-((h/2)*BProz/100));
      stift.drawLine(b+(b/40), (h/2)-((h/2)*BProz/100), b+(b/80), ((h/2)-((h/2)*BProz/100))+(b/80));
      stift.drawLine(b+(b/40), (h/2)-((h/2)*BProz/100), b+3*(b/80), ((h/2)-((h/2)*BProz/100))+(b/80));
      
    }
    
    //Raster nachzeichnen, weil sonst durchschnitten
    raster(h,b,stift);
    
  }
  
  
  private static void raster(int h, int b, Graphics stift){
    
    
    
    //Bedingung, dass nur wenn aktiv und radiert ausgef�hrt
    if(ras==true && rad==true){
      
      stift.setColor(Color.lightGray);
      //vertikale Rasterlinien zeichnen
      for (int g=0; g<b; g=g+50) {        
        stift.drawLine(g, 0, g, h);        
      } 
      //horizontale Rasterlinien zeichnen
      for (int g=0; g<h; g=g+50) {
        stift.drawLine(0, g, b, g);
      } 
      
      
      
    }
    
  }  
    
    
  private static double iBerechnung(double i){
    
    //Schrittweite anpassen, dass es nicht zu langsam ist
    if(i<1.5){
      i=i+0.01;
    }else if(i>=1.5 && i<2.5){
      i=i+0.02;
    }else if(i>=2.5 && i<4.5){    
      i=i+0.04;
    }else if(i>=4.5 && i<8.5){
      i=i+0.08;
    }else if(i>=8.5 && i<16.5){
      i=i+0.16;
    }else if(i>=16.5 && i<32.5){
      i=i+0.32;
    }else if(i>=32.5 && i<64.5){
      i=i+0.64;
    }else if(i>=64.5 && i<128.5){
      i=i+1.28;
    }else if(i>=128.5){
      i=i+2.56;
    }
    
    return i;
  }  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
