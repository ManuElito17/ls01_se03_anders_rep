import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.util.*;
import java.lang.Math.*;
import java.applet.*;
//import javafx.util.Pair;


 
public class Gitterformatierung 
extends JFrame  {
  
   Container c;
  
  //Materialien f�r ComboBox
  private static final String[] Materialien = {
    "Aluminium", "Bismut", "Gold", "Kupfer", "Platin", "Silber", "Zink"  
    //Germanium erg�nzen 
  }; 
  //private static double Aluminium = -3.5 * Math.pow(10,-11);
  
  //Globale Variablen
  public static JPanel Label1 = new JPanel();
  public static JCheckBox RasterBox = new JCheckBox("Raster");
  public static JCheckBox VektorBox = new JCheckBox("Vektorpfeile");
  private static JComboBox MaterialBox = new JComboBox(Materialien);
  private static JSlider MagnetfeldSlider = new JSlider();
  private static JTextField EingabeMagnetfeld = new JTextField(1);
  private static JSlider ElektrischesFeldSlider = new JSlider();
  private static JTextField EingabeElektrischesFeld = new JTextField(1);
  private static JSlider DickeFolieSlider = new JSlider();
  private static JTextField EingabeDickeFolie = new JTextField(1);
  private static JButton StartButton = new JButton("Start");
  public static String Magnetfeld;
  public static String ElektrischesFeld;
  private static String DickeFolie;
  private static String Start;
  private static String WerteMagnetfeld;
  private static String WerteElektrischesFeld;
  private static String WerteDickeFolie;
  private static Double Hallkonstante; 
  
  
  private static JTextField Formel = new JTextField();
  private static JTextField FormelWerte = new JTextField("U(Hall)=");
  private static JTextField LabelR = new JTextField("R(H)=");
  private static JTextField LabelU = new JTextField("U(Hall)=");   
  private static Double Rechnung;
  
  //Zeichenbereich / Elektronen definieren, um ihn hinzuf�gen und entfernen zu k�nnen
  public static Elektronen zBer;
  
  //Boolean, um zu sehen, ob Knopf schon gedr�ckt wurde - also zBer schon existiert
  public static boolean existent = false;
  
  public static void main(String[] args){
    Gitterformatierung fenster = new Gitterformatierung();
    //fenster.setTitle("Hall-Effekt");
    fenster.setSize(800,500);   
    fenster.setVisible(true);                              
    //fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
    
    
    //Change Listener f�r Slider St�rke Magnetfeld
    MagnetfeldSlider.addChangeListener(new ChangeListener(){ 
      public void stateChanged(ChangeEvent ce) {
        
        Gitterformatierung.Magnetfeld = Integer.toString( ((JSlider) ce.getSource()).getValue());
        //System.out.println(((JSlider) ce.getSource()).getValue());
        //System.out.println (Gitterformatierung.Magnetfeld);
        
        EingabeMagnetfeld.setText(Gitterformatierung.Magnetfeld);
        
      }
    });
    
    //Change Listener f�r Slider St�rke Elektrisches Feld
    ElektrischesFeldSlider.addChangeListener(new ChangeListener(){ 
      public void stateChanged(ChangeEvent ce) {
        
        Gitterformatierung.ElektrischesFeld = Integer.toString( ((JSlider) ce.getSource()).getValue());
        //System.out.println(((JSlider) ce.getSource()).getValue());
        //System.out.println (Gitterformatierung.ElektrischesFeld);
        
        EingabeElektrischesFeld.setText(Gitterformatierung.ElektrischesFeld);
        
      }
    });
    
    //Change Listener f�r Slider Dicke der Folie
    DickeFolieSlider.addChangeListener(new ChangeListener(){ 
      public void stateChanged(ChangeEvent ce) {
        
        Gitterformatierung.DickeFolie = Integer.toString( ((JSlider) ce.getSource()).getValue());
        //System.out.println(((JSlider) ce.getSource()).getValue());
        //System.out.println (Gitterformatierung.DickeFolie);
        
        EingabeDickeFolie.setText(Gitterformatierung.DickeFolie);
        
      }
    });
    
    
    //Action Listener f�r Textfeld Eingabe Magnetfeld 
    EingabeMagnetfeld.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) {
        
        WerteMagnetfeld=EingabeMagnetfeld.getText();
        MagnetfeldSlider.setValue(Integer.parseInt(WerteMagnetfeld));
        //System.out.println("Werte vom Magnetfeld " + WerteMagnetfeld);
      }
    });
    
    
    //Action Listener f�r Textfeld Eingabe elektrisches Feld
    EingabeElektrischesFeld.addActionListener(new ActionListener() {;
      public void actionPerformed(ActionEvent ae){
        
        WerteElektrischesFeld=EingabeElektrischesFeld.getText();
        ElektrischesFeldSlider.setValue(Integer.parseInt(WerteElektrischesFeld));
        //System.out.println("Werte vom elektrischen Feld " + WerteElektrischesFeld);
        
      }
    }); 
    
    
    //Action Listener f�r Textfeld Eingabe der Dicke der Folie
    EingabeDickeFolie.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae){
        
        WerteDickeFolie=EingabeDickeFolie.getText();
        DickeFolieSlider.setValue(Integer.parseInt(WerteDickeFolie));
        //System.out.println("Werte f�r Dicke der Folie " + WerteDickeFolie);
        
      }
    });
    
    
    //Action Listener f�r Button Start
    StartButton.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent ae){
        
        //falls Werte noch nicht festgelegt, auf Startwert gesetzt
        if( Gitterformatierung.ElektrischesFeld == null ){
          Gitterformatierung.ElektrischesFeld = "1";
        }
        if( Gitterformatierung.Magnetfeld == null ){
          Gitterformatierung.Magnetfeld = "1";
        }
        if( Gitterformatierung.DickeFolie == null ){
          Gitterformatierung.DickeFolie = "3";
        }
        
        
        //System.out.println("RasterBox " + RasterBox.isSelected());  
        //System.out.println("VektorBox " + VektorBox.isSelected());
        //System.out.println("Art des Materials: " + MaterialBox.getSelectedItem());
        //System.out.println("Slider Staerke Magnetfeld: " + Gitterformatierung.Magnetfeld);
        //System.out.println("Slider Staerke elektrisches Feld: " + Gitterformatierung.ElektrischesFeld);
        //System.out.println("Slider Dicke der Folie: " + DickeFolie);
        
        
        //HallKonstante
        if (MaterialBox.getSelectedItem() == "Aluminium") {
          Hallkonstante = -3.5 * Math.pow(10,-11);
        } 
        else if (MaterialBox.getSelectedItem() == "Bismut") {
          Hallkonstante = -6 * Math.pow(10,-7);
        } 
        else if (MaterialBox.getSelectedItem() == "Gold") {
          Hallkonstante = -7.2  * Math.pow(10,-11);
        }
        else if (MaterialBox.getSelectedItem() == "Kupfer") {
          Hallkonstante = -5.3 * Math.pow(10,-11);
        } 
        else if (MaterialBox.getSelectedItem() == "Platin") {
          Hallkonstante = -2 * Math.pow(10,-11);
        }
        else if (MaterialBox.getSelectedItem() == "Silber") {
          Hallkonstante = -8.9 * Math.pow(10,-11);
        }
        else if (MaterialBox.getSelectedItem() == "Zink") {
          Hallkonstante = 6.4 * Math.pow(10,-11);
        } 
        else {
          Hallkonstante = 0.0;
        } 
        
        
        if( Gitterformatierung.ElektrischesFeld.isEmpty() == true ){
          Gitterformatierung.ElektrischesFeld = "0";
        }
        
        
        FormelWerte.setText("U(Hall)=R(H) " + "* " + Gitterformatierung.ElektrischesFeld + " * " +  Gitterformatierung.Magnetfeld + "/ " + Gitterformatierung.DickeFolie);
        LabelR.setText("R(H)= " + Hallkonstante);
        
        Rechnung = Hallkonstante * Double.parseDouble(Gitterformatierung.ElektrischesFeld) * Double.parseDouble(Gitterformatierung.Magnetfeld) / Double.parseDouble(Gitterformatierung.DickeFolie);
        //System.out.println("Rechnung " + Rechnung);
        
        LabelU.setText("U(Hall)= " + Rechnung);  
        
        
        
        
        //Fenster �ffnen, um Graph zu zeichen
        Elektronen.main();
        
        
        
      }
    });
  }
  
  public Gitterformatierung() {
    super("Hall Effekt");
    addWindowListener(new Schliessen(true));
    GridBagLayout gbl = new GridBagLayout();
    GridBagConstraints gbc= new GridBagConstraints();
    setLayout(gbl);
    
    
    
    Label1.setOpaque(true);
    Label1.setForeground(Color.RED);
    Label1.setBackground(Color.WHITE);
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 600;
    gbc.gridheight = 400;
    gbc.weightx = 80;
    gbc.weighty = 80;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(Label1, gbc);
    add(Label1);
    
    
    //Label f�r die Formel
    Formel.setOpaque(true);
    gbc.gridx = 0;
    gbc.gridy = 400;
    gbc.gridwidth = 1;
    gbc.gridheight = 2;
    gbc.weightx = 1;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(Formel, gbc);
    Formel.setText("U(Hall)=R(H)* I * B / d");
    add(Formel);
    
    
    //Label f�r Werte der Formel
    FormelWerte.setOpaque(true);
    gbc.gridx = 1;
    gbc.gridy = 400;
    gbc.gridwidth = 1;
    gbc.gridheight = 2; 
    gbc.weightx = 2;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(FormelWerte, gbc);
    add(FormelWerte);
    
    
    //Label f�r R(H)
    LabelR.setOpaque(true);
    gbc.gridx = 0;
    gbc.gridy = 402;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(LabelR, gbc);
    add(LabelR);
    
    
    //Label f�r U(Hall)
    LabelU.setOpaque(true);
    gbc.gridx = 1;
    gbc.gridy = 402;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(LabelU, gbc);
    add(LabelU);
    
    
    //CheckBox f�r die Raster
    RasterBox.setOpaque(true);
    gbc.gridx = 600;
    gbc.gridy = 0;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(RasterBox,gbc);
    add(RasterBox);
    
    
    //CheckBox f�r Einschalten/ Ausschalten Vektorpfeile
    VektorBox.setOpaque (true);
    gbc.gridx = 600;
    gbc.gridy = 1;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(VektorBox, gbc);
    add(VektorBox);
    
    
    //Label St�rke Magnetfeld
    JLabel Magnetfeld = new JLabel("St�rke vom Magnetfeld");
    Magnetfeld.setOpaque(true);
    gbc.gridx = 600;
    gbc.gridy = 6;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(Magnetfeld, gbc);
    add(Magnetfeld);
    
    //Schieberegler St�rke Magnetfeld
    MagnetfeldSlider.setOpaque(true);
    gbc.gridx = 600;
    gbc.gridy = 7;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(MagnetfeldSlider, gbc);
    //Minimum festlegen
    MagnetfeldSlider.setMinimum(1);
    //Maximum festlegen
    MagnetfeldSlider.setMaximum(21);
    //Abstand gro�er Striche festlegen
    MagnetfeldSlider.setMajorTickSpacing(5);
    //Abstand kleiner Striche festlegen
    MagnetfeldSlider.setMinorTickSpacing(1);
    //Striche sichtbar machen
    MagnetfeldSlider.setPaintTicks(true);
    //Standartbeschriftung festlegen und sichtbar machen
    MagnetfeldSlider.createStandardLabels(5);
    MagnetfeldSlider.setPaintLabels(true);
    //Standartwert festlegen
    MagnetfeldSlider.setValue(1);
    add(MagnetfeldSlider);
    
    //Eingabefeld St�rke Magnetfeld
    EingabeMagnetfeld.setOpaque(true);
    gbc.gridx = 601;
    gbc.gridy = 7;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(EingabeMagnetfeld, gbc);
    add(EingabeMagnetfeld);
    
    
    //Label Spannung (St�rke elektrisches Feld)
    JLabel ElektrischesFeld = new JLabel("St�rke elektrisches Feld");
    ElektrischesFeld.setOpaque(true);
    gbc.gridx = 600 ;
    gbc.gridy = 8;
    gbc.gridwidth = 1;
    gbc.gridheight =1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(ElektrischesFeld, gbc);
    add(ElektrischesFeld);
    
    //Schieberegler Spannung  (St�rke elektrisches Feld)
    ElektrischesFeldSlider.setOpaque(true);
    gbc.gridx = 600;
    gbc.gridy = 9;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(ElektrischesFeldSlider, gbc);
    //Minimum vom JSlider festlegen
    ElektrischesFeldSlider.setMinimum(1);
    //Maximum vom JSlider festlegen
    ElektrischesFeldSlider.setMaximum(21);     
    //Abstand der Gro�en Striche festlegen
    ElektrischesFeldSlider.setMajorTickSpacing(5);
    //Abstand der kleinen Striche festlegen
    ElektrischesFeldSlider.setMinorTickSpacing(1);
    //Striche sichtbar machen
    ElektrischesFeldSlider.setPaintTicks(true);
    //Standartbeschriftung festlegen und sichbar machen
    ElektrischesFeldSlider.createStandardLabels(5);
    ElektrischesFeldSlider.setPaintLabels(true);
    //Startwert festlegen
    ElektrischesFeldSlider.setValue(1);
    add(ElektrischesFeldSlider); 
    
    //Eingabe Spannung (St�rke elektrisches Feld)
    EingabeElektrischesFeld.setOpaque(true);
    gbc.gridx = 601;
    gbc.gridy = 9;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(EingabeElektrischesFeld, gbc);
    add(EingabeElektrischesFeld); 
    
    
    //ComboBox f�r Art des Materials
    MaterialBox.setOpaque(true);
    gbc.gridx = 600;
    gbc.gridy = 400;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(MaterialBox, gbc);
    add(MaterialBox);
    
    
    //Label Dicke der Folie
    JLabel DickeFolie = new JLabel("Dicke der Folie in mm");
    DickeFolie.setOpaque(true);
    gbc.gridx = 600;
    gbc.gridy = 401;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(DickeFolie, gbc);
    add(DickeFolie);
    
    //Schieberegler f�r Dicke der Folie
    DickeFolieSlider.setOpaque(true);
    gbc.gridx = 600;
    gbc.gridy = 402;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 20;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(DickeFolieSlider, gbc);
    //Minimum vom JSlider festlegen
    DickeFolieSlider.setMinimum(1);
    //Maximum vom JSlider festlegen 
    DickeFolieSlider.setMaximum(5);
    //Abstand der Striche festlegen und sichtbar machen
    DickeFolieSlider.setMajorTickSpacing(1);
    DickeFolieSlider.setPaintTicks(true);
    //Standartbeschriftung festlegen und sichtbar machen
    DickeFolieSlider.createStandardLabels(1);
    DickeFolieSlider.setPaintLabels(true);
    //Startwert festlegen
    DickeFolieSlider.setValue(3);
    add(DickeFolieSlider); 
    
    
    //Eingabe Dicke der Folie
    EingabeDickeFolie.setOpaque(true);
    gbc.gridx = 601;
    gbc.gridy = 402;
    gbc.gridwidth = 1;
    gbc.gridheight = 1;
    gbc.weightx = 1;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(EingabeDickeFolie, gbc);
    add(EingabeDickeFolie); 
    
    
    //Button Start
    StartButton.setOpaque(true);
    gbc.gridx = 601;
    gbc.gridy = 1;
    gbc.gridwidth =1;
    gbc.gridheight = 1;
    gbc.weightx = 1;
    gbc.fill = GridBagConstraints.BOTH;
    gbl.setConstraints(StartButton, gbc);
    add(StartButton);
    
                             
    //Ausgabe der Werte
    //System.out.println("RasterBox " + RasterBox.isSelected());  
    //System.out.println("VektorBox " + VektorBox.isSelected());
    //System.out.println("Art des Materials: " + MaterialBox.getSelectedItem());
    
  }  
}
 



//Klasse zum Fensterschlie�en
class Schliessen extends WindowAdapter {
  private boolean exitSystem;
  
  /**
  * Erzeugt einen WindowClosingAdapter zum Schliessen 
  * des Fensters. Ist exitSystem true, wird das komplette
  * Programm beendet.
  */
  public Schliessen(boolean exitSystem){
    this.exitSystem = exitSystem;
    
  }
  
  /**
  * Erzeugt einen WindowClosingAdapter zum Schliessen 
  * des Fensters. Das Programm wird nicht beendet.
  */
  public Schliessen(){
    this(true);
    
  }  
  
  public void windowClosing(WindowEvent event){
    event.getWindow().setVisible(false);
    event.getWindow().dispose();
    if(exitSystem){
      System.exit(0);
    }  
  }
  
  
  } 




