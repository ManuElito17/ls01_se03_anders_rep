﻿import java.lang.reflect.Array;
import java.util.Scanner;

class Fahrkartenautomat	{
    public static void main(String[] args)	{
      
    	while(1<2) {
    	//kein Übergabeparameter
    	//Rückgabewert ist "zuZahlenderBetrag"
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	
    	//Übergeben von "zuZahlenderBetrag"
    	//Rückgabewert ist "rückgabebetrag"
    	double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	
    	//kein Übergabeparameter
    	//kein Rückgabewert
    	fahrkartenAusgeben();		
    	
    	//Übergeben von "rückgabebetrag" 
    	//kein Rückgabewert
    	rueckgeldAusgeben(rückgabebetrag);	
    	
    	//Leerzeile schaffen
    	System.out.println("");
    	}
    	
    }
    
    
    
    //Fahrkartenbestellung erfassen und zuZahlenderBetrag zurückgeben
    public static double fahrkartenbestellungErfassen() {
    	
    	double zuZahlenderBetrag; 
        int anzahl;
        Scanner tastatur = new Scanner(System.in);
        String Fahrkartenbezeichnung[] = new String[11];
        double Fahrkartenpreise [] = new double[11];
        int eingabe;        
        
        System.out.println("Fahrkartenbestellvorgang: ");
        System.out.println("=========================");
        System.out.println(" ");
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus: ");
        System.out.println("");
        System.out.printf("%-20s%-40s%20s%n","Auswahlnummer",  "Bezeichnung" , "Preis in Euro");
        
        Fahrkartenpreise[1] = 2.90;
        Fahrkartenpreise[2] = 3.30;
        Fahrkartenpreise[3] = 3.60;
        Fahrkartenpreise[4] = 1.60;	
        Fahrkartenpreise[5] = 8.60;
        Fahrkartenpreise[6] = 9.00;
        Fahrkartenpreise[7] = 9.60;
        Fahrkartenpreise[8] = 23.50;
        Fahrkartenpreise[9] = 24.30;
        Fahrkartenpreise[10] = 24.90;
    	
    	Fahrkartenbezeichnung[1] = "Einzelfahrschein Berlin AB ";
        Fahrkartenbezeichnung[2] = "Einzelfahrschein Berlin BC ";
        Fahrkartenbezeichnung[3] = "Einzelfahrschein Berlin ABC ";
        Fahrkartenbezeichnung[4] = "Kurzstrecke ";	
    	Fahrkartenbezeichnung[5] = "Tageskarte Berlin AB ";
    	Fahrkartenbezeichnung[6] = "Tageskarte Berlin BC ";
    	Fahrkartenbezeichnung[7] = "Tageskarte Berlin ABC ";
    	Fahrkartenbezeichnung[8] = "Kleingruppen-Tageskarte Berlin AB ";
    	Fahrkartenbezeichnung[9] = "Kleingruppen-Tageskarte Berlin BC ";
    	Fahrkartenbezeichnung[10] = "Kleingruppen-Tageskarte Berlin ABC ";
    	
    	for(int i=1; i<Fahrkartenbezeichnung.length; i++) {
    		System.out.printf("%-20s", "[" +  i + "]");
    		System.out.printf("%-40s",Fahrkartenbezeichnung[i] );
    		System.out.printf("%20.2f%n", Fahrkartenpreise[i]);
    	}
        
    	System.out.println("");
        System.out.print("Ihre Wahl: ");
        eingabe = tastatur.nextInt();
        System.out.println(" ");
        
        while(eingabe > 10 || eingabe < 1) {
        	System.out.println(">>falsche Eingabe<<");
        	System.out.print("Ihre Wahl: ");
        	eingabe = tastatur.nextInt();
        }
        zuZahlenderBetrag = Fahrkartenpreise[eingabe] * 100;
        
        System.out.print("Anzahl Tickets: ");
        anzahl = tastatur.nextInt();
        zuZahlenderBetrag = anzahl * zuZahlenderBetrag;
        
        return zuZahlenderBetrag;    //double
    }
    
    
    //Geldeinwurf erfassen und rückgabebetrag zurückgeben
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;
        Scanner tastatur = new Scanner(System.in);
        
        eingezahlterGesamtbetrag = 0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)	{
     	  
     	   System.out.printf( "%s%.2f%s\n" , "Noch zu zahlen: " , (zuZahlenderBetrag - eingezahlterGesamtbetrag)/100   , " Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   eingeworfeneMünze = eingeworfeneMünze*100 ;
           eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze;
        }
       
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
    	return rückgabebetrag;	//double
    }
    

    //Fahrkarten ausgeben
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
    	
        for (int i = 0; i < 8; i++)	{
        	
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} 
           catch (InterruptedException e) {
 			e.printStackTrace();
           }
        }
        
        System.out.println("\n\n");
    }
    
    
    //Rückgeld ausgeben
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	   
         if(rückgabebetrag > 0)	{
       	   System.out.printf("%s%.2f%s%n","Der Rückgabebetrag in Höhe von ", rückgabebetrag/100 , " EURO" );
       	   System.out.println("wird in folgenden Münzen ausgezahlt:");
           
       	   // 2 EURO-Münzen   
       	   while(rückgabebetrag >= 200)	{
           	   System.out.println("2.00 EURO");
   	          rückgabebetrag = rückgabebetrag - 200;
           }
       	   // 1 EURO-Münzen
           while(rückgabebetrag >= 100)	{
           	  System.out.println("1.00 EURO");
   	          rückgabebetrag = rückgabebetrag - 100;
           }
           // 50 CENT-Münzen
           while(rückgabebetrag >= 50)	{
           	  System.out.println("50 CENT");
   	          rückgabebetrag = rückgabebetrag - 50;
           }
           // 20 CENT-Münzen
           while(rückgabebetrag >= 20)	{
           	  System.out.println("20 CENT");
    	      rückgabebetrag = rückgabebetrag - 20;
           }
           // 10 CENT-Münzen
           while(rückgabebetrag >= 10)	{
           	  System.out.println("10 CENT");
   	          rückgabebetrag = rückgabebetrag - 10;
           }
           // 5 CENT-Münzen
           while(rückgabebetrag >= 5)	{
           	  System.out.println("5 CENT");
    	      rückgabebetrag = rückgabebetrag - 5;
           }
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                             "vor Fahrtantritt entwerten zu lassen!\n"+
                             "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
}