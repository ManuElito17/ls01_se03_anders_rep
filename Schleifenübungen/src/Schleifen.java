import java.util.Scanner;

public class Schleifen {

	public static void main(String[] args) {
		
		
		//temperaturumrechnung();
		addition();
		
				
		

	}
	
	public static void temperaturumrechnung() {
		
		double celsiusstart = 0;
		double celsiusende = 0;
		double schrittweite = 0;
		double fahrenheit = 0;
		Scanner  tastatur = new Scanner(System.in);
		
		System.out.println("Bitte den Startwert in Celsius eingeben: ");
		celsiusstart = tastatur.nextDouble();
		System.out.println("Bitte den Endwert in Celsius eingeben: ");
		celsiusende = tastatur.nextDouble();
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben:");
		schrittweite = tastatur.nextDouble();
		
		while(celsiusstart<=celsiusende) {
			fahrenheit = (celsiusstart * 9 / 5) + 32;
			System.out.printf("%.2f%s%.2f%s%n" ,celsiusstart , "�C  =  " , fahrenheit , "�F");
			celsiusstart = celsiusstart + schrittweite;
		}
	}
	
	
	public static void addition() {
		
		int begrenzungszahl = 0;
		int i;
		int wertA = 0;
		int wertB = 0;
		int wertC = 0;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte einen begrenzenden Wert ein: ");
		begrenzungszahl  = tastatur.nextInt();
		
		//Addition A			n
		i = 0;
		while(i <= begrenzungszahl) {
			wertA = wertA + i;
			i++;
		}
		System.out.println("Die Summe f�r A betr�gt: " + wertA);
		
		//Addition B			2n
		i = 0;
		while(i <= begrenzungszahl) {
			wertB = wertB + i;
			i = i + 2;
		}
		System.out.println("Die Summe f�r B betr�gt: " + wertB);
		
		//Addition C			2n+1
		i = 1;
		while(i<=begrenzungszahl) {
			wertC = wertC + i;
			i = i+2;
		}
		System.out.println("Die Summe f�r C betr�gt: " + wertC);
		
	}
	

}
